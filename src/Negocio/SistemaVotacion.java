package Negocio;
import Entidad.*;
import ufps.util.colecciones_seed.*;
public class SistemaVotacion {
    
    private ListaCD<Departamento> dptos=new ListaCD();
    private ListaCD<Persona> personas=new ListaCD();
    private Cola<Notificacion> notificaciones=new Cola();

    public SistemaVotacion() {
    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public void setDptos(ListaCD<Departamento> dptos) {
        this.dptos = dptos;
    }

    public ListaCD<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ListaCD<Persona> personas) {
        this.personas = personas;
    }

    public Cola<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(Cola<Notificacion> notificaciones) {
        this.notificaciones = notificaciones;
    }
    
    
    
    
    
    
}
